CXXFLAGS=-g -pedantic -Wall -Wextra -Werror

all: copy_as

copy_as: main.o
	g++ $(CXXFLAGS) -o copy_as main.o

main.o: main.cpp copy_as.hpp

.PHONY: all
