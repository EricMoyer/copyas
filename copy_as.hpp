#pragma once
#include <type_traits>
#include <iterator>
#include <algorithm>

/// Void type for template metaprogramming
template<typename> struct SVoid { typedef void type; };

/// Value is true if push_back (and begin) are available on \a T
///
/// Example:
/// \code
///  cout << "vector push_back:" << SCanPushBack<vector<string>>::value << endl
/// \endcode
///
/// First part of a metaprogramming pair ... this is for the false case
template<typename T, typename Sfinae = void>
struct SCanPushBack: std::false_type {};

/// Value is true if push_back (and begin) are available on \a T
///
/// Second part of a metaprogramming pair ... this is for the true case
///
/// See other part for example.
///
/// This had to use begin because I couldn't get declval to work with
/// T::value_type. Someone else can fix it if it becomes a problem.
template<typename T>
struct SCanPushBack<
    T
    , typename SVoid<
        decltype( std::declval<T&>().push_back(*(std::declval<T&>().begin())) )
    >::type
>: std::true_type {};

/// Returns a good insert iterator to use for insertions into \a c
///
/// This is one of two implementations. This one is chosen if the
/// container does not have a push_back method
template <typename Container,
          typename std::enable_if<! SCanPushBack<Container>::value, int>::type = 0>
std::insert_iterator<Container> g_GoodInserter(Container& c)
{
    return std::inserter(c, c.begin());
}

/// Returns a good insert iterator to use for insertions into \a c
///
/// This is one of two implementations. This one is chosen if the
/// container has a push_back method
template <typename Container,
          typename std::enable_if<SCanPushBack<Container>::value, int>::type = 0>
std::back_insert_iterator<Container> g_GoodInserter(Container& c)
{
    return std::back_inserter(c);
}

/// Copy \a in to a new container of type \a OutContainer
///
/// Example:
/// \code
/// SCopyAs<set<Uint8>>::copy(refsnp_data_serial.GetPubmed_links())
/// \endcode
///
/// \param in the container to copy
///
/// \tparam InContainer The type of the input container
///
/// \tparam OutContainer The type of the output container
template<class OutContainer>
struct SCopyAs{
    template<class InContainer>
    static OutContainer copy(const InContainer& in){
        OutContainer out;
        std::copy(in.begin(), in.end(), g_GoodInserter(out));
        return out;
    }
};
