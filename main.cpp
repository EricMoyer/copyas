#include "copy_as.hpp"
#include <iostream>
#include <vector>
#include <set>
#include <list>
#include <string>

template<class Container>
void print(const Container&c){
    for(const auto& e: c){
        std::cout << " " << e;
    }
    std::cout << std::endl;
}

int main(int, char**){
    using namespace std;

    vector<string> v{"a","bra","cad","ab","ra"};
    set<string> s{"Al","a","ka","zam"};

    cout << "vector push_back:" << SCanPushBack<vector<string> >::value << endl;
    cout << "set push_back:" << SCanPushBack<set<string> >::value << endl;

    g_GoodInserter(v);
    g_GoodInserter(s);

    cout << "Copied to list" << endl;
    print(SCopyAs<list<string>>::copy(v));

    cout << "Copied to set" << endl;
    print(SCopyAs<set<string>>::copy(v));

    return 0;
}
